package com.tingting.sideProject.login;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tingting.sideProject.service.MemberJpaService;
import com.tingting.sideProject.service.PersonJpaRepository;
import com.tingting.sideProject.service.RoleJpaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginApplicationTests {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MemberJpaService memberJpaService;
	
	@Autowired
	PersonJpaRepository personJpaRepository;
	
	@Autowired
	RoleJpaService roleJpaService;
	
	@Test
	public void contextLoads() {
		//logger.info("All members -> {}", memberJpaService.getAllMember());
		//logger.info("All persons -> {}", personJpaRepository.findAll());
		
		//logger.info("member 10001's Roles -> {}", roleJpaService.getRolesByMemberId(10001));
	}

}