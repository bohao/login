/*create table UserInfo 
(
    id integer not null,
    name varchar(255) not null,
    username varchar(20) not null,
    password varchar(20) not null,
    primary key (id)
);*/

insert into Member (id, name, username, password)
values(10001,'Joe','admin','admin');
insert into Member (id, name, username, password)
values(10002,'Andy','test','test');

insert into Role (id, member_id, role)
values(1,10001,'ROLE_ADMIN');
insert into Role (id, member_id, role)
values(2,10002,'ROLE_MEMBER');



insert into Person (id, name, location, birthdate)
values(10001,'Joe','NewYork',sysdate());
insert into Person (id, name, location, birthdate)
values(10002,'Cathy','Tokyo',sysdate());
insert into Person (id, name, location, birthdate)
values(10003,'Alice','Taipei',sysdate());