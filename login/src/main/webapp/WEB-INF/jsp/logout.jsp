<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
    <div>
        <div>
            <h1>Spring Boot JSP Example</h1>
            <h2>${message}</h2>
             
            Click on this <strong><a href=index>link</a></strong> to index.
            Click on this <strong><a href="login">link</a></strong> to login.
            Click on this <strong><a href="member">link</a></strong> to member page.
            Click on this <strong><a href="admin">link</a></strong> to admin page.
        </div>
    </div>
</body>
</html>