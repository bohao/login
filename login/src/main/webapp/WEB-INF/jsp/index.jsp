<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
    <div>
        <div>
            <h1>Spring Boot JSP Example</h1>
            <h2>Hello ${message}</h2>
            ${session == null}
            Click on this <strong><a href="login">link</a></strong> to login.<br>
            Click on this <strong><a href="member">link</a></strong> to member page.<br>
            Click on this <strong><a href="admin">link</a></strong> to admin page.<br>
            Click on this <strong><a href="logout">link</a></strong> to logout page.
        </div>
    </div>
</body>
</html>