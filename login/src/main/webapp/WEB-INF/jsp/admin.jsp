<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
    <div>
        <div>
            <h1>Spring Boot JSP Example</h1>
            <h2>Hello ${message}</h2>
            Click on this <strong><a href="index">link</a></strong> to index page.<br>
            Click on this <strong><a href="member">link</a></strong> to member page.<br>
            Click on this <strong><a href="logout">link</a></strong> to logout.
        </div>
    </div>
</body>
</html>