<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en">
<body>
    <div>
        <div>
            <h1>Spring Boot JSP Example</h1>
            <h2>Hello ${message}</h2>
            
            Click on this <strong><a href="admin">link</a></strong> to admin page.
            Click on this <strong><a href="logout">link</a></strong> to logout.<br>
        </div>
    </div>
</body>
</html>