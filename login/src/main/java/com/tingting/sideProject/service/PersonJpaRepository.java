package com.tingting.sideProject.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tingting.sideProject.entity.Person;

@Repository
@Transactional
public class PersonJpaRepository {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    //connect to database
    @PersistenceContext
    EntityManager entityManager;

    public Person findById(int id) {
        return entityManager.find(Person.class, id);
    }
    
    public List<Person> findAll() {
        TypedQuery<Person> namedQuery = entityManager.createNamedQuery("find_all_person",  Person.class);
        return namedQuery.getResultList();
    }
}
