package com.tingting.sideProject.service;

import java.util.List;

import com.tingting.sideProject.entity.Member;

public interface MemberJpaService {
    public Member getMemberById(int id);
    public List<Member> getMemberByUsernameAndPassword(String username, String password);
    public List<Member> getAllMember();
}
