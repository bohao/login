package com.tingting.sideProject.service;

import java.util.List;

import com.tingting.sideProject.entity.Role;

public interface RoleJpaService {
    public List<Role> getRolesByMemberId(Integer id);
}
