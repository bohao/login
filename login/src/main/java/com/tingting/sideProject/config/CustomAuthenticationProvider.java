package com.tingting.sideProject.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.tingting.sideProject.entity.Member;
import com.tingting.sideProject.entity.Role;
import com.tingting.sideProject.service.MemberJpaService;
import com.tingting.sideProject.service.RoleJpaService;

@Configuration
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MemberJpaService memberJpaService;

    @Autowired
    RoleJpaService roleJpaService;

    @Autowired
    UserDetailsService demoUserDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        logger.info("username : " + username + " password : " + password);
        List<Member> memberList = memberJpaService.getMemberByUsernameAndPassword(username, password);
        Member member = null;
        if (memberList != null && memberList.size() == 1) {
            member = memberList.get(0);
            logger.info("menberId : " + member.getId());

            List<Role> roleList = roleJpaService.getRolesByMemberId(member.getId());

            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

            logger.info("role : " + roleList.get(0).getRole());
            grantedAuthorities.add(new SimpleGrantedAuthority(roleList.get(0).getRole()));

            Authentication auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);

            return auth;
        } else throw new UsernameNotFoundException("can not find this account");
    // return new UsernamePasswordAuthenticationToken(null, null, null);
//        UserDetails userDetails = demoUserDetailsService.loadUserByUsername(authentication.getName());
//        
//        if(userDetails == null) {
//            throw new UsernameNotFoundException("找不到該用戶");
//        }
//        logger.info(userDetails.getUsername());
//        logger.info(userDetails.getPassword());
//        logger.info(authentication.getName());
//        logger.info(authentication.getCredentials().toString());
//        if(!userDetails.getPassword().equals("{noop}" + authentication.getCredentials().toString())) {
//            throw new BadCredentialsException("密碼錯誤");
//        }
//        userDetails.getAuthorities().forEach(a -> System.out.println(a.getAuthority()));
//        return new UsernamePasswordAuthenticationToken(userDetails,userDetails.getPassword(),userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
        // return
        // (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
