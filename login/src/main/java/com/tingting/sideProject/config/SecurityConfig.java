package com.tingting.sideProject.config;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import com.tingting.sideProject.handler.CustomLogoutSuccessHandler;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    UserDetailsService userServiceDetails;
    
    @Autowired
    CustomAuthenticationProvider authenticationProvider;
    
    @Autowired
    InMemoryAuthenticationProvider inMemoryAuthenticationProvider;
    
    @Autowired
    UserDetailsService demoUserDetailsService;
    
    @Autowired
    CustomAccessDeniedExceptionHandler accessDeniedExceptionHandler;
    
    @Autowired
    CustomAuthenticationFailureHandler authenticationFailureHandler;
    
    @Autowired
    CustomLogoutSuccessHandler logoutSuccessHandler;

//    @Bean
//    DaoAuthenticationProvider daoAuthenticationProvider(){
//    DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
//    daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
//    daoAuthenticationProvider.setUserDetailsService(demoUserDetailsService);
//    return daoAuthenticationProvider;
//    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        logger.info("configure...AuthenticationManagerBuilder");
        
        //自行定義認證
        auth.authenticationProvider(authenticationProvider);
        
//        try {
//            //自行定義認證
//            auth.authenticationProvider(authenticationProvider);
//        } catch(Exception e) {
//        	logger.info("123");
//        	logger.info(e.getMessage());
//        }
        //auth.userDetailsService(demoUserDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        logger.info("configure...HttpSecurity");
        http.csrf().disable().authorizeRequests()
                //.anyRequest() // 對象為所有網址
                //.authenticated() // 存取必須通過驗證
                .antMatchers("/index").permitAll()
                .antMatchers("/h2_console/**").permitAll()
                .antMatchers("/member").hasAnyRole("MEMBER", "ADMIN")
                .antMatchers("/admin").hasRole("ADMIN")
                //.exceptionHandling().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/401"))
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedExceptionHandler)//.accessDeniedPage("/accessDenied")
                .and()
                .formLogin() // 若未不符合authorize條件，則產生預設login表單
                .failureHandler(authenticationFailureHandler)
                .and()
                .logout()
                .logoutUrl("logout")
                .and()
                
//                .invalidateHttpSession(true)
//                .deleteCookies("JSESSIONID")
                //.logoutSuccessHandler(logoutSuccessHandler)
                ;
//                .and().httpBasic(); // 產生基本表單
        // 禁用默認的csrf驗證
        //http.csrf().disable();

        
//        http.authorizeRequests()
//        // we need config just for console, nothing else
//        .antMatchers("/h2_console/**").permitAll();
//        // this will ignore only h2-console csrf, spring security 4+
//        http.csrf().ignoringAntMatchers("/h2-console/**");
//        //this will allow frames with same origin which is much more safe
//        http.headers().frameOptions().sameOrigin();

//        http.csrf().disable().authorizeRequests()
//                // .antMatchers("/**").authenticated()
//                .and().formLogin().loginPage("/login").permitAll();
//        .successHandler(new MySavedRequestAwareAuthenticationSuccessHandler())
//        .failureHandler(new SimpleUrlAuthenticationFailureHandler())
//        .and()
//        .logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
//        .and()
//        .exceptionHandling().authenticationEntryPoint(new Http401AuthenticationEntryPoint("login first"))
//        .accessDeniedHandler(new AccessDeniedHandlerImpl());
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        // 不攔截靜態資源的訪問
        //web.ignoring().antMatchers("/js/**", "/css/**", "/images/**");
    }
    
//    @Override
//    protected AuthenticationManager authenticationManager() throws Exception {
//    ProviderManager authenticationManager = new ProviderManager(Arrays.asList(inMemoryAuthenticationProvider,authenticationProvider));
//    //不擦除認證密碼，擦除會導致TokenBasedRememberMeServices因為找不到Credentials再呼叫UserDetailsService而丟擲UsernameNotFoundException
//    authenticationManager.setEraseCredentialsAfterAuthentication(false);
//   
//    return authenticationManager;
//    }
//    /**
//    * 這裡需要提供UserDetailsService的原因是RememberMeServices需要用到
//    * @return
//    */
//    @Override
//    protected UserDetailsService userDetailsService() {
//    return userServiceDetails;
//    }
}
