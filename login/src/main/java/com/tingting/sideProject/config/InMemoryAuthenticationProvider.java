package com.tingting.sideProject.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class InMemoryAuthenticationProvider implements AuthenticationProvider {

	private final String adminName = "root";
	private final String adminPassword = "root";
	//根使用者擁有全部的許可權
	private final List<GrantedAuthority> authorities = Arrays.asList(
	new SimpleGrantedAuthority("ROLE_ADMIN"),
	new SimpleGrantedAuthority("ROLE_MEMBER"));
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	if(isMatch(authentication)){
	User user = new User(authentication.getName(),authentication.getCredentials().toString(),authorities);
	return new UsernamePasswordAuthenticationToken(user,authentication.getCredentials(),authorities);
	}
	return null;
	}
	@Override
	public boolean supports(Class<?> authentication) {
	return true;
	}
	private boolean isMatch(Authentication authentication){
	if(authentication.getName().equals(adminName)&&authentication.getCredentials().equals(adminPassword))
	return true;
	else
	return false;
	}

}
