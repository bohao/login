package com.tingting.sideProject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.tingting.sideProject.service.MemberJpaService;
import com.tingting.sideProject.service.RoleJpaService;

@SpringBootApplication
public class LoginApplication implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    MemberJpaService memberJpaService;
    
    @Autowired
    RoleJpaService roleJpaService;
    
    public static void main(String[] args) {
        SpringApplication.run(LoginApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    	//logger.info("member 10001's Roles -> {}", roleJpaService.getRolesByMemberId(10001));
        //logger.info("All users -> {}", userJpaRepository.findAll());
        //logger.info("User id 10001 -> {}", userJpaRepository.findById(10001));
        //personJpaRepository.deleteById(10002);
        //logger.info("insert User 10004 -> {}", personJpaRepository.insert(new Person("Perter", "Beijin", new Date())));
        //logger.info("update User 10003 -> {}", personJpaRepository.update(new Person(10003, "Alice2", "Taoyan", new Date())));
    }
    
}
