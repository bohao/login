package com.tingting.sideProject.dao;

import java.util.List;

import com.tingting.sideProject.entity.Role;

public interface RoleDao {
	public List<Role> findByMemberId(Integer memberId) ;
}
