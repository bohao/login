package com.tingting.sideProject.dao;

import java.util.List;

import com.tingting.sideProject.entity.Member;

public interface MemberDao {
    public Member findById(int id);
    public Member findByUsername(String username);
    public List<Member> findByUsernameAndPassword(String username, String password) ;
    public List<Member> findAll();
}
