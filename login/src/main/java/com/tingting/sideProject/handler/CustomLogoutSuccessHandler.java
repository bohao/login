package com.tingting.sideProject.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@Configuration
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        if (authentication != null && authentication.getDetails() != null) {
               try {
                   logger.info("onLogoutSuccess");
                   request.getSession().invalidate();
                   new SecurityContextLogoutHandler().logout(request, response, authentication);
                   //you can add more codes here when the user successfully logs out,
                   //such as updating the database for last active.
               } catch (Exception e) {
                   e.printStackTrace();
               }
           }
        logger.info("response 200");
        response.setStatus(HttpServletResponse.SC_OK);
        
        //response.sendRedirect("/logout");
    }
}
