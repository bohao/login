package com.tingting.sideProject.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tingting.sideProject.dao.MemberDao;
import com.tingting.sideProject.dao.RoleDao;
import com.tingting.sideProject.entity.Member;
import com.tingting.sideProject.entity.Role;
import com.tingting.sideProject.service.MemberJpaService;

@Repository
@Transactional
public class MemberJpaServiceImpl implements MemberJpaService {
    @Autowired
    MemberDao memberDao;
    
    @Autowired
    RoleDao roleDao;
    
    @Override
    public Member getMemberById(int id) {
        return memberDao.findById(id);
    }

    @Override
    public List<Member> getMemberByUsernameAndPassword(String username, String password) {
        return memberDao.findByUsernameAndPassword(username, password);
    }

    @Override
    public List<Member> getAllMember() {
        return memberDao.findAll();
    }
}
