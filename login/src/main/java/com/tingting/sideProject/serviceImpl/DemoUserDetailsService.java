package com.tingting.sideProject.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tingting.sideProject.dao.MemberDao;
import com.tingting.sideProject.entity.Member;

@Service
public class DemoUserDetailsService implements UserDetailsService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private MemberDao memberDao;
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("loadUserByUsername access db user.");
		logger.info("username : " + username);
		Member member = memberDao.findByUsername(username);

		logger.info("member == null : " + (member == null));
        if (member == null) {
            throw new UsernameNotFoundException(username + " not found");
        }
        logger.info(member.toString());

        UserDetails userDetails = User.builder()
                .username(member.getName())
                .password("{noop}" + member.getPassword())
                .roles("ADMIN").build();

        return userDetails;
	}
	
//	@Override
//    public UserDetails loadUserByUsername(String username)
//            throws UsernameNotFoundException {
//        logger.info("loadUserByUsername fix user.");
//		logger.info("username : " + username);
//		
//        UserDetails userDetails = User.builder()
//                .username("user123")
//                .password("{noop}user123") // 密碼前面加上"{noop}"使用NoOpPasswordEncoder，也就是不對密碼進行任何格式的編碼。
//                .roles("USER")
//                .build();
//        
//        return userDetails;
//    }

}
