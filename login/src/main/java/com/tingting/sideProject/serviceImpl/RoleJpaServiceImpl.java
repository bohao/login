package com.tingting.sideProject.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tingting.sideProject.dao.RoleDao;
import com.tingting.sideProject.entity.Role;
import com.tingting.sideProject.service.RoleJpaService;

@Repository
@Transactional
public class RoleJpaServiceImpl implements RoleJpaService {
    @Autowired
    RoleDao roleDao;

    @Override
    public List<Role> getRolesByMemberId(Integer id) {
        return roleDao.findByMemberId(id);
    }
}
