package com.tingting.sideProject.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javassist.tools.web.BadHttpRequest;

@Controller
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = { "", "/", "index" })
    public String index(Map<String, Object> model) {
        logger.info("forward to index.jsp");
        model.put("message", "You are in index page !!");
        return "index";
    }

    @RequestMapping("/member")
    public String member(Map<String, Object> model) {
        logger.info("forward to member.jsp");
        model.put("message", "You are in member page !!");
        return "member";
    }

    @RequestMapping("/admin")
    public String admin(Map<String, Object> model) {
        logger.info("forward to admin.jsp");
        model.put("message", "You are in admin page !!");
        return "admin";
    }

    @RequestMapping("/accessDenied")
    public String accessDenied(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) {
        try {
            if (authentication == null) {
                logger.info("unAuthenticated");
                request.getSession().invalidate();
                new SecurityContextLogoutHandler().logout(request, response, authentication);
                return "unAuthenticated";
            } else if (authentication != null && authentication.getDetails() != null
                    && authentication.getAuthorities().size() > 0) {
                logger.info("accessDenied");
                return "accessDenied";
            }
        } catch (AuthenticationException e) {
            return "badRequest";
        }
        return "";
    }
}
