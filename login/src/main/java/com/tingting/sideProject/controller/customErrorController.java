//package com.tingting.sideProject.controller;
//
//import javax.servlet.http.HttpServletResponse;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.web.servlet.error.ErrorController;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
////@RestController
//@Controller
//public class customErrorController implements ErrorController {
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//	@RequestMapping("/error")
//	public String handleError(HttpServletResponse response) {
//		//ModelAndView modelAndView = new ModelAndView();
//		logger.info("handleError");
//		logger.info("status :" + response.getStatus());
//		String page = null;
//
//		if(response.getStatus() == HttpStatus.BAD_REQUEST.value()) {
//			//modelAndView.setViewName("badRequest");
//			page =  "badRequest";
//		}
////		else if(response.getStatus() == HttpStatus.FORBIDDEN.value()) {
////			modelAndView.setViewName("error-403");
////		}
////		else if(response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
////			modelAndView.setViewName("error-500");
////		}
////		else {
////			modelAndView.setViewName("error");
////		}
//
//		return page;
//	}
//	
//	@GetMapping("/400")
//    public String badRequest() {
//        return "badRequest";
//    }
//	@GetMapping("/404")
//    public String notFound() {
//        return "error/404";
//    }
//	@GetMapping("/500")
//    public String serverError() {
//        return "error/500";
//    }
//
//	@Override
//	public String getErrorPath() {
//		return "/error";
//	}
//}
