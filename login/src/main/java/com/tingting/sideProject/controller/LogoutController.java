package com.tingting.sideProject.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LogoutController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/logout")
    public String logout(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) {
        model.put("message", "You have logout !!");
        if (authentication != null && authentication.getDetails() != null) {
            try {
                logger.info("onLogoutSuccess");
                request.getSession().invalidate();
                new SecurityContextLogoutHandler().logout(request, response, authentication);
                // you can add more codes here when the user successfully logs out,
                // such as updating the database for last active.
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("response 200");
        response.setStatus(HttpServletResponse.SC_OK);
        logger.info("forward to logout.jsp");
        return "logout";
    }
}
