package com.tingting.sideProject.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.tingting.sideProject.dao.MemberDao;
import com.tingting.sideProject.entity.Member;

@Repository
@Transactional
public class MemberDaoImpl implements MemberDao {
    
    //connect to database
    @PersistenceContext
    EntityManager entityManager;
    
    @Override
    public Member findById(int id) {
        return entityManager.find(Member.class, id);
    }
    @Override
    public List<Member> findByUsernameAndPassword(String username, String password) {
        CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Member> cq = qb.createQuery(Member.class);
        Root<Member> root = cq.from(Member.class);
        cq.where(qb.equal(root.get("username"), username), 
                qb.equal(root.get("password"), password)
        );
        return entityManager.createQuery(cq).getResultList();
    }
    @Override
    public List<Member> findAll() {
        TypedQuery<Member> namedQuery = entityManager.createNamedQuery("find_all_member", Member.class);
        return namedQuery.getResultList();
    }
	@Override
	public Member findByUsername(String username) {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Member> cq = qb.createQuery(Member.class);
        Root<Member> root = cq.from(Member.class);
        cq.where(qb.equal(root.get("username"), username));
        List<Member> list = entityManager.createQuery(cq).getResultList();
        return list != null && list.size() > 0 ? list.get(0) : null;
	}
}
