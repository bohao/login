package com.tingting.sideProject.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tingting.sideProject.dao.RoleDao;
import com.tingting.sideProject.entity.Role;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    //connect to database
    @PersistenceContext
    EntityManager entityManager;
    
    @Override
    public List<Role> findByMemberId(Integer memberId) {
        CriteriaBuilder qb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Role> cq = qb.createQuery(Role.class);
        Root<Role> root = cq.from(Role.class);
        cq.where(qb.equal(root.get("memberId"), memberId));
        
        logger.info("role size : " + entityManager.createQuery(cq).getResultList().size());
        entityManager.createQuery(cq).getResultList().forEach(r -> System.out.println(r.getRole()));
        return entityManager.createQuery(cq).getResultList();
    }

}
